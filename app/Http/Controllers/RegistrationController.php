<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use App\Mail\Welcome;
use App\User;
use App\Http\Requests\RegistrationRequest;

class RegistrationController extends Controller
{
    public function __constructor()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function create()
    {
        return view('registrations.create');
    }

    public function store(RegistrationRequest $request)
    {

   	    $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => Hash::make(request('password')),
        ]);

        auth()->login($user);

        session()->flash('message', 'thanks for signing up');

        \Mail::to($user)->send(new Welcome($user));

        return redirect()->home();
    }

}
