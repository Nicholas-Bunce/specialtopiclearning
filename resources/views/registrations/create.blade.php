@extends ('layout.master')


@section ('content')
    <div>
        <h1>Registration</h1>
    </div>

    <form action="/register" method="POST">
    {{ csrf_field() }}

    <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" id="name" name="name" required>
    </div>
    
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" name="email" required>
    </div>

    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" id="password" name="password" required>
    </div>

    <div class="form-group">
            <label for="password_confirmation">Password Conformation</label>
            <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
        </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Registor</button>
    </div>

    <div class="form-group">
        @include ('layout.error')
    </div>


</form>
@endsection