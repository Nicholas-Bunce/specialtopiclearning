@component('mail::message')
# Introduction

thanks for registoring

@component('mail::button', ['url' => 'https://laracasts.com'])
start browsing
@endcomponent

@component('mail::panel', ['url' => ''])
helpful stuff
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
