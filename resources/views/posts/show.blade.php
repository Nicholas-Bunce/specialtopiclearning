@extends('layout.master')

@section ('content')>


        <h1>{{ $post->title }}</h1>

        @if (count($post->tags))
            <ul>
                @foreach ($post->tags as $tag)
                    <li>{{ $tag->name }}</li>
                @endforeach
            </ul>
        @endif    

        {{ $post->body}}

        <hr>
        <li class="list-group">
            <div class="comments">
                @foreach ($post->comments as $comment)
                    <li class="list-group-item">
                        <strong> {{ $comment->created_at->diffForHumans() }} </strong>: &nbsp;
                        {{ $comment->body }}
                    </li>    
                @endforeach
            </div>
        </li>

        <div class="card col-md-8">
            <div class="card-block">
                <form method="POST" action="/posts/{{ $post->id }}/comments">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <textarea name="body" placeholder="Your comment here" class="form-control" required></textarea>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Add comment</button>
                    </div>
                </form>
                @include ('layout.error')
            </div>
        </div>

        @include ('layout.sidebar')




@endsection