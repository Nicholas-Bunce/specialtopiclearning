@extends ('layout.master')

@section ('content')

    <h1>Create a post</h1>

    <form method="POST" action="/posts">
    {{ csrf_field() }}
  <div class="form-group">
    <label for="title">Title:</label>
    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" required>
    <small id="TitleHelp" class="form-text text-muted">Start the rant today</small>
  </div>
  <div class="form-group">
    <label for="body">Body</label>
    <textarea class="form-control" id="body" placeholder="Body" name="body" required></textarea> 
  </div>

  <button type="submit" class="btn btn-primary">Publish</button>

  @include('layout.error')
  
</form>



@endsection