 <h3 class="pb-3 mb-4 font-italic border-bottom">
              From the Firehose
            </h3>
            <div class="blog-post">
            <h2 class="blog-post-title"> <a href="Post/{{ $post->id }}"> {{ $post->title }} </a> </h2>
            <p class="blog-post-meta">{{ $post->created_at->toFormattedDateString() }} by <a href="#">{{ $post->user->name }}</a></p>
    
                 {{ $post->body }}
              </div><!-- /.blog-post -->