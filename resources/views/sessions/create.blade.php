@extends ('layout.master')


@section ('content')
    <div>
        <h1>Login</h1>
    </div>

    <form action="/login" method="POST">
    {{ csrf_field() }}
    
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" name="email">
    </div>

    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" id="password" name="password">
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Login</button>
    </div>

    <div class="form-group">
            @include ('layout.error')
    </div>
    
</form>
@endsection